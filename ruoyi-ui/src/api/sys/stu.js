import request from '@/utils/request'

// 查询学生信息列表
export function listStu(query) {
  return request({
    url: '/sys/stu/list',
    method: 'get',
    params: query
  })
}

// 查询学生信息详细
export function getStu(studentId) {
  return request({
    url: '/sys/stu/' + studentId,
    method: 'get'
  })
}

// 新增学生信息
export function addStu(data) {
  return request({
    url: '/sys/stu',
    method: 'post',
    data: data
  })
}

// 修改学生信息
export function updateStu(data) {
  return request({
    url: '/sys/stu',
    method: 'put',
    data: data
  })
}

// 删除学生信息
export function delStu(studentId) {
  return request({
    url: '/sys/stu/' + studentId,
    method: 'delete'
  })
}

// 导出学生信息
export function exportStu(query) {
  return request({
    url: '/sys/stu/export',
    method: 'get',
    params: query
  })
}