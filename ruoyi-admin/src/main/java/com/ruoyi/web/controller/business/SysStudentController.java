package com.ruoyi.web.controller.business;

import java.util.Arrays;
import java.util.List;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import com.ruoyi.common.annotation.Log;
import com.ruoyi.common.core.controller.BaseController;
import com.ruoyi.common.core.domain.AjaxResult;
import com.ruoyi.common.enums.BusinessType;
import com.ruoyi.business.domain.SysStudent;
import com.ruoyi.business.service.ISysStudentService;
import com.ruoyi.common.utils.poi.ExcelUtil;
import com.ruoyi.common.core.page.TableDataInfo;

import javax.annotation.Resource;

/**
 * 学生信息Controller
 * 
 * @author lxy
 * @date 2022-03-25
 */
@Api(value = "学生信息", tags = {"学生信息模块"})
@RestController
@RequestMapping("/business/student")
public class SysStudentController extends BaseController {
    @Resource
    private ISysStudentService sysStudentService;

    @ApiOperation("查询学生信息列表")
    @PreAuthorize("@ss.hasPermi('business:student:list')")
    @GetMapping("/list")
    public TableDataInfo list(SysStudent sysStudent) {
        startPage();
        List<SysStudent> list = sysStudentService.queryList(sysStudent);
        return getDataTable(list);
    }

    @ApiOperation("导出学生信息")
    @PreAuthorize("@ss.hasPermi('business:student:export')")
    @Log(title = "学生信息", businessType = BusinessType.EXPORT)
    @GetMapping("/export")
    public AjaxResult export(SysStudent sysStudent) {
        List<SysStudent> list = sysStudentService.queryList(sysStudent);
        ExcelUtil<SysStudent> util = new ExcelUtil<>(SysStudent.class);
        return util.exportExcel(list, "学生信息数据");
    }

    @ApiOperation("获取学生信息详细信息")
    @PreAuthorize("@ss.hasPermi('business:student:query')")
    @GetMapping(value = "/{studentId}")
    public AjaxResult getInfo(@PathVariable("studentId") Integer studentId) {
        return AjaxResult.success(sysStudentService.getById(studentId));
    }

    @ApiOperation("新增学生信息")
    @PreAuthorize("@ss.hasPermi('business:student:add')")
    @Log(title = "学生信息", businessType = BusinessType.INSERT)
    @PostMapping
    public AjaxResult add(@RequestBody SysStudent sysStudent) {
        return toAjax(sysStudentService.save(sysStudent));
    }

    @ApiOperation("修改学生信息")
    @PreAuthorize("@ss.hasPermi('business:student:edit')")
    @Log(title = "学生信息", businessType = BusinessType.UPDATE)
    @PutMapping
    public AjaxResult edit(@RequestBody SysStudent sysStudent) {
        return toAjax(sysStudentService.updateById(sysStudent));
    }

    @ApiOperation("删除学生信息")
    @PreAuthorize("@ss.hasPermi('business:student:remove')")
    @Log(title = "学生信息", businessType = BusinessType.DELETE)
	@DeleteMapping("/{studentIds}")
    public AjaxResult remove(@PathVariable Integer[] studentIds) {
        return toAjax(sysStudentService.removeByIds(Arrays.asList(studentIds)));
    }
}
