package com.ruoyi.business.domain;

import java.util.Date;

import com.fasterxml.jackson.annotation.JsonFormat;

import java.io.Serializable;
import java.util.Map;

import com.ruoyi.common.annotation.Excel;
import com.baomidou.mybatisplus.annotation.*;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.experimental.Accessors;
import io.swagger.annotations.ApiModelProperty;
import com.ruoyi.business.core.BaseEntity;

/**
 * 学生信息对象 sys_student
 *
 * @author lxy
 * @date 2022-03-25
 */
@Data
@NoArgsConstructor
@Accessors(chain = true)
@TableName(value = "sys_student")
public class SysStudent extends BaseEntity {

    @TableId(type = IdType.AUTO)
    private Integer studentId;

    @ApiModelProperty("学生名称")
    @Excel(name = "学生名称")
    private String studentName;

    @ApiModelProperty("年龄")
    @Excel(name = "年龄")
    private Integer studentAge;

    @ApiModelProperty("爱好")
    @Excel(name = "爱好", readConverterExp = "0=代码,1=音乐,2=电影")
    private String studentHobby;

    @ApiModelProperty("性别")
    @Excel(name = "性别", readConverterExp = "0=男,1=女,2=未知")
    private String studentSex;

    @ApiModelProperty("状态")
    @Excel(name = "状态", readConverterExp = "0=正常,1=停用")
    private String studentStatus;

    @ApiModelProperty("生日")
    @JsonFormat(pattern = "yyyy-MM-dd")
    @Excel(name = "生日", width = 30, dateFormat = "yyyy-MM-dd")
    private Date studentBirthday;

}
