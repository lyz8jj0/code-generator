package com.ruoyi.business.mapper;

import com.ruoyi.business.domain.SysStudent;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

/**
 * 学生信息Mapper接口
 * 
 * @author lxy
 * @date 2022-03-25
 */

public interface SysStudentMapper extends BaseMapper<SysStudent> {

}
