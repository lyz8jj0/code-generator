package com.ruoyi.business.service;

import java.util.List;
import com.ruoyi.business.domain.SysStudent;
import com.baomidou.mybatisplus.extension.service.IService;

/**
 * 学生信息Service接口
 * 
 * @author lxy
 * @date 2022-03-25
 */
public interface ISysStudentService extends IService<SysStudent> {

    /**
     * 查询学生信息列表
     *
     * @param sysStudent 学生信息
     * @return 学生信息集合
     */
    List<SysStudent> queryList(SysStudent sysStudent);

}
