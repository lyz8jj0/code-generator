package com.ruoyi.business.service.impl;

import java.util.List;
import org.springframework.stereotype.Service;
import com.ruoyi.common.utils.StringUtils;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.ruoyi.business.mapper.SysStudentMapper;
import com.ruoyi.business.domain.SysStudent;
import com.ruoyi.business.service.ISysStudentService;
import org.springframework.transaction.annotation.Transactional;
import javax.annotation.Resource;
import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;

/**
 * 学生信息Service业务层处理
 *
 * @author lxy
 * @date 2022-03-25
 */
@Service
@Transactional(rollbackFor = Exception.class)
public class SysStudentServiceImpl extends ServiceImpl<SysStudentMapper, SysStudent> implements ISysStudentService {

    @Resource
    private SysStudentMapper sysStudentMapper;

    /**
     * 查询学生信息列表
     *
     * @param sysStudent 学生信息
     * @return 学生信息
     */
    @Override
    public List<SysStudent> queryList(SysStudent sysStudent) {
        LambdaQueryWrapper<SysStudent> queryWrapper = new LambdaQueryWrapper<>();

        if (StringUtils.isNotEmpty(sysStudent.getStudentName())){
            queryWrapper.like(SysStudent::getStudentName, sysStudent.getStudentName());
        }
        if (StringUtils.isNotNull(sysStudent.getStudentAge())){
            queryWrapper.eq(SysStudent::getStudentAge, sysStudent.getStudentAge());
        }
        if (StringUtils.isNotEmpty(sysStudent.getStudentHobby())){
            queryWrapper.eq(SysStudent::getStudentHobby, sysStudent.getStudentHobby());
        }
        if (StringUtils.isNotEmpty(sysStudent.getStudentSex())){
            queryWrapper.eq(SysStudent::getStudentSex, sysStudent.getStudentSex());
        }
        if (StringUtils.isNotEmpty(sysStudent.getStudentStatus())){
            queryWrapper.eq(SysStudent::getStudentStatus, sysStudent.getStudentStatus());
        }
        if (StringUtils.isNotNull(sysStudent.getStudentBirthday())){
            queryWrapper.eq(SysStudent::getStudentBirthday, sysStudent.getStudentBirthday());
        }
        return this.list(queryWrapper);
    }

}
